#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <unicode/brkiter.h>
#include <unicode/locid.h>
#include <unicode/uclean.h>
#include <unicode/utypes.h>
#include <unicode/utext.h>

namespace po = boost::program_options;

const char * const program_name = "unicode-graphemes";

static bool show_glyphs;

void fatal(const std::string & message) {
  std::cerr << program_name << ": " << message << "\n";
  std::cerr << "Try '" << program_name << " --help' for more information.\n";
  exit(EXIT_FAILURE);
}

void display_bytes(const std::string & line, int64_t start_index, int64_t end_index) {
  for (int64_t i = start_index; i < end_index; ++i) {
    char c = line[i];

    // do not display control characters
    if (iscntrl(c)) {
      continue;
    }

    std::cout << c;
  }
}

void display_file(std::istream & input_stream) {
  icu::Locale locale = icu::Locale::getUS();
  UErrorCode status = U_ZERO_ERROR;
  icu::BreakIterator * bi = icu::BreakIterator::createCharacterInstance(locale, status);
  if (! U_SUCCESS(status)) {
    fatal("Could not create BreakIterator");
  }

  int32_t character_name_buffer_length = 16;
  char * character_name_buffer = (char *) malloc(character_name_buffer_length);
  if (character_name_buffer == nullptr) {
    fatal("Out of memory");
  }

  std::string line;
  while (std::getline(input_stream, line)) {
    // we want to display the line including the LF at the end
    if (! input_stream.eof()) {
      line += '\n';
    }

    // https://unicode-org.github.io/icu/userguide/strings/utext.html
    UErrorCode status = U_ZERO_ERROR;
    UText * ut = nullptr;
    ut = utext_openUTF8(ut, line.c_str(), -1, &status);
    if (! U_SUCCESS(status)) {
      fatal("Error decoding UTF-8");
    }

    bi->setText(ut, status);
    if (! U_SUCCESS(status)) {
      fatal("Could not set text for BreakIterator");
    }

    int32_t grapheme_cluster_start_boundary = -1;
    for (int32_t p = bi->first(); p != icu::BreakIterator::DONE; p = bi->next()) {
      int32_t grapheme_cluster_end_boundary = p;
      if (grapheme_cluster_start_boundary >= 0) {
        // iterate through the code points for this grapheme cluster
        bool indent = false;
        int64_t code_point_start_boundary = grapheme_cluster_start_boundary;
        utext_setNativeIndex(ut, grapheme_cluster_start_boundary);
        for (;;) {
          UChar32 code_point = utext_next32(ut);
          if (code_point == U_SENTINEL) {
            fatal("Expected a code point but found end of string");
          }

          int64_t code_point_end_boundary = utext_getNativeIndex(ut);

          if (code_point_start_boundary == grapheme_cluster_start_boundary && code_point_end_boundary < grapheme_cluster_end_boundary) {
            // this is a grapheme cluster with multiple code points - print out the grapheme cluster
            std::cout << "Grapheme cluster";
            if (show_glyphs) {
              std::cout << ": ";
              display_bytes(line, grapheme_cluster_start_boundary, grapheme_cluster_end_boundary);
            }
            std::cout << std::endl;
            indent = true;
          }

          // get the character name
          UErrorCode status = U_ZERO_ERROR;
          int32_t length = u_charName(code_point, U_UNICODE_CHAR_NAME, character_name_buffer, character_name_buffer_length, &status);

          /*
          Note that U_SUCCESS(status) will be true even if u_charName() failed to write the trailing NUL character because the buffer was too short!
          So we need to check the returned length first.
          */
          if (character_name_buffer_length <= length) {
            // reallocate buffer
            character_name_buffer_length = length + 1;
            character_name_buffer = (char *) realloc(character_name_buffer, character_name_buffer_length);
            if (character_name_buffer == nullptr) {
              fatal("Out of memory");
            }

            // call u_charName() again
            status = U_ZERO_ERROR;
            length = u_charName(code_point, U_UNICODE_CHAR_NAME, character_name_buffer, character_name_buffer_length, &status);

            // sanity-check the returned length
            if (character_name_buffer_length != length + 1) {
              fatal(std::string("Unexpected character name length: ") + std::to_string(length));
            }
          }

          // check the status of the last u_charName() call
          if (! U_SUCCESS(status)) {
            fatal("Error obtaining character name");
          }

          // print out the code point
          if (indent) {
            std::cout << '\t';
          }
          std::cout << "U+" << std::setfill('0') << std::setw(4) << std::uppercase << std::hex << code_point;
          std::cout << " ";
          std::cout << std::dec << code_point;
          std::cout << " ";
          std::cout << character_name_buffer;
          if (show_glyphs) {
            std::cout << ": ";
            display_bytes(line, code_point_start_boundary, code_point_end_boundary);
          }
          std::cout << std::endl;

          if (code_point_end_boundary == grapheme_cluster_end_boundary) {
            break;
          }
          else if (code_point_end_boundary > grapheme_cluster_end_boundary) {
            fatal("Code point extends past the end of the grapheme cluster");
          }

          code_point_start_boundary = code_point_end_boundary;
        }
      }

      grapheme_cluster_start_boundary = grapheme_cluster_end_boundary;
    }

    utext_close(ut);
  }

  free(character_name_buffer);

  delete bi;
}

int main(int argc, char ** argv) {
  po::options_description visible_options("Options");
  visible_options.add_options()
    ("help,h", "display this help and exit")
    ("show-glyphs", "(attempt to) display glyphs")
  ;

  po::options_description hidden_options("Hidden options");
  hidden_options.add_options()
    ("input-file", po::value<std::string>(), "input file")
  ;

  po::options_description all_options("All options");
  all_options.add(visible_options).add(hidden_options);

  po::positional_options_description positional_options;
  positional_options.add("input-file", 1);

  po::variables_map vm;
  try {
    po::store(po::command_line_parser(argc, argv).options(all_options).positional(positional_options).run(), vm);
  }
  catch (po::too_many_positional_options_error & e) {
    fatal(e.what());
  }
  catch (po::unknown_option & e) {
    fatal(e.what());
  }
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << "Usage: " << program_name << " [OPTION]... [FILE]\n";
    std::cout << "Display graphemes in FILE (must be UTF-8).\n";
    std::cout << "\n";
    std::cout << "With no FILE, or when FILE is -, read standard input.\n";
    std::cout << "\n";
    std::cout << visible_options << "\n";
    return 0;
  }

  if (vm.count("show-glyphs")) {
    show_glyphs = true;
  }
  else {
    show_glyphs = false;
  }

  if (vm.count("input-file") == 0) {
    display_file(std::cin);
  }
  else {
    std::string input_file = vm["input-file"].as<std::string>();
    if (input_file == "-") {
      display_file(std::cin);
    }
    else {
      std::ifstream f(input_file);
      if (! f.is_open()) {
        fatal(std::string("Could not open file ") + input_file);
      }
      display_file(f);
    }
  }

  u_cleanup();

  return 0;
}
