CXXFLAGS = -Wall -Wextra -std=c++11 -pedantic
program = unicode-graphemes
objects = src/unicode-graphemes.o
icu_libs := $(shell pkg-config --libs icu-uc)

all: $(program)

$(program): $(objects)
	$(CXX) $(CXXFLAGS) -o $@ $^ -lboost_program_options $(icu_libs)

clean:
	$(RM) $(program) $(objects)

test:
	cd ../unicode-graphemes-tests && UNICODE_GRAPHEMES=../../unicode-graphemes/unicode-graphemes ./all-tests.sh

cppcheck:
	cppcheck --std=c++11 src

scan-build:
	scan-build -o scan-build make

.PHONY: cppcheck scan-build
