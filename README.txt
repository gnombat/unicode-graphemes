This program displays graphemes (including grapheme clusters) in UTF-8 text.

An example:

$ env printf '\xc3\xa9\x65\xcc\x81' | ./unicode-graphemes
U+00E9 233 LATIN SMALL LETTER E WITH ACUTE
Grapheme cluster
	U+0065 101 LATIN SMALL LETTER E
	U+0301 769 COMBINING ACUTE ACCENT
$ env printf '\xc3\xa9\x65\xcc\x81' | ./unicode-graphemes --show-glyphs
U+00E9 233 LATIN SMALL LETTER E WITH ACUTE: é
Grapheme cluster: é
	U+0065 101 LATIN SMALL LETTER E: e
	U+0301 769 COMBINING ACUTE ACCENT: ́

This program requires the following libraries:

* ICU
* Boost Program Options

On a Debian or Ubuntu system, you can probably install these with this command:

  apt install libicu-dev libboost-program-options-dev

Once you have the required libraries installed, you should be able to compile
the program:

  make

This will create a binary named "unicode-graphemes".  You can run this on any
UTF-8 text file:

  ./unicode-graphemes FILE.txt

By default, the output contains only Unicode code points (in hexadecimal and
decimal) and character names. You can use the --show-glyphs option to include
the actual glyphs in the output. (Your mileage may vary depending on what
terminal you are using, what fonts you have installed, etc.)

  ./unicode-graphemes --show-glyphs FILE.txt

This program is free software. See the file "LICENSE.txt" for license terms.
